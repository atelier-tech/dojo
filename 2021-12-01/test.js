const fs = require('fs');
const { splitColumns, splitLines, readMatriz, readFileAsMatriz, weatherSpread, minWeatherSpread, minBalance, weather, football } = require('./');

test('adds 1 + 2 to equal 3', () => {
    expect(1 + 2).toBe(3);
});

test('teste exploratorio do fs', done => {
    fs.readFile('test.txt', 'utf8', function (err, data) {
        if (err) {
            done(err);
        }
        expect(data).toBe('conteudo');
        done();
    });
});

test('teste exploratorio do fs com quebra de linha', done => {
    fs.readFile('multiple_lines.txt', 'utf8', function (err, data) {
        if (err) {
            done(err);
        }
        expect(data).toBe('line 1\nline 2');
        done();
    });
});


test('separar colunas', () => {
    const linha = "  Dy MxT   MnT";
    const columns = splitColumns(linha);
    expect(columns).toStrictEqual(["Dy", "MxT", "MnT"]);
});

test("separar colunas com outra string", () => {
    const linha = "  a b   c";
    const columns = splitColumns(linha);
    expect(columns).toStrictEqual(['a', 'b', 'c']);
});

test("separar linhas em outro array", () => {
    const string = 'line 1\nline 2';
    const lines = splitLines(string);
    expect(lines).toStrictEqual(['line 1', 'line 2']);
});

test("separar linhas em outro outro array", () => {
    const string = 'line 3\n\n\nline 4\nline7\n\nline8\n \n';
    const lines = splitLines(string);
    expect(lines).toStrictEqual(['line 3', 'line 4', 'line7', 'line8']);
});

test("ler matriz", () => {
    const string = [
        ["a", "b", "c"].join(" "),
        ["1", "2", "3"].join(" "),
    ].join("\n");
    const matriz = readMatriz(string);
    expect(matriz).toStrictEqual([
        ["a", "b", "c"],
        ["1", "2", "3"],
    ]);
});


test("ler matriz cavala", () => {
    const string = [
        ["a", "b", "c", "g", "l"].join(" "),
        ["1", "2"].join(" "),
        ["1", "2", "3"].join(" "),
    ].join("\n");
    const matriz = readMatriz(string);
    expect(matriz).toStrictEqual([
        ["a", "b", "c", "g", "l"],
        ["1", "2"],
        ["1", "2", "3"]
    ]);
});

test("ler o arquivo multiple_line como matriz ", done => {
    readFileAsMatriz('multiple_lines.txt', 'utf8', function (err, matriz) {
        if (err) {
            done(err);
        }

        expect(matriz).toStrictEqual([['line', '1'], ['line', '2']]);
        done();
    });
});

test("ler o arquivo weather.dat como matriz", done => {
    readFileAsMatriz('weather.dat', 'utf8', function (err, matriz) {
        if (err) {
            done(err);
        }

        expect(matriz.length).toBe(32);
        expect(matriz[0].length).toStrictEqual(17);
        done();
    });
})

test("Calcular variação termica", () => {
    const spreadResult = weatherSpread([1, 9, 2]);
    expect(spreadResult).toBe(7);
});

test("Calcular variação termica com entrada", () => {
    const spreadResult = weatherSpread([1, 15, 9]);
    expect(spreadResult).toBe(6);
});

test("Calcular variação termica com vetor em string", () => {
    const spreadResult = weatherSpread(['1', '26', '35']);
    expect(spreadResult).toBe(9);
});

test("Calcular variação termica com vetor em string ignorando o asterisco", () => {
    const spreadResult = weatherSpread(['1', '26*', '35']);
    expect(spreadResult).toBe(9);
});

test("Calcular variação termica com vetor em string ignorando o varios ateriscoso e caracteres especiais", () => {
    const spreadResult = weatherSpread(['1', '26*&[]%$#', '35']);
    expect(spreadResult).toBe(9);
});


describe("Array.sort", () => {
    test('sort', () => {
        const result = [3, 2, 1, 5, 4].sort((a, b) => a - b);
        expect(result).toStrictEqual([1, 2, 3, 4, 5]);
    });
})


describe('minWeatherSpread', () => {
    test("matriz 1", () => {
        const matriz = [
            ["1", "25", "16", "55"],
        ]
        const result = minWeatherSpread(matriz);
        expect(result).toBe("1");
    });
    test("matriz grande", () => {
        const matriz = [
            ["1", "1", "16", "55"],
            ["2", "2", "16", "55"],
            ["3", "3", "16", "55"],
            ["4", "4", "16", "55"],
        ];
        const result = minWeatherSpread(matriz);
        expect(result).toBe("4");
    });

    test("matriz grande melhorada", () => {
        const matriz = [
            ["1", "1", "16", "55"],
            ["2", "2", "16", "55"],
            ["3", "3", "16", "55"],
            ["4", "4", "16", "55"],
            ["5", "1", "16", "55"]
        ];
        const result = minWeatherSpread(matriz);
        expect(result).toBe("4");
    });
});

test("retornar valor minimo dentro do arquivo", done => {
    weather(output => {
        expect(output).toBe("14");
        done();
    })
});


test("Ler arquivo football.dat", done => {
    football( output => {
        expect(output).toBe("Aston_Villa")
        done();
    })
});


describe("minBalance", () => {
    test("matriz 1", () => {
        const matriz = [
            // Team            P     W    L   D    F      A     Pts
            ["1.", "Arsenal"       ,"38","26","9","3","79","-","36","87"],
        ];
        const result = minBalance(matriz)
        expect(result).toBe("Arsenal");
    });
});
