const fs = require('fs');

const DAY_NUMBER = 0;
const MAX_TEMP = 1;
const MIN_TEMP = 2;

const TEAM_NAME = 1;
const F_COLUMN = 6;
const A_COLUMN = 8;


const readFileAsMatriz =
    (file, encoding, fn) =>
         fs.readFile( file, encoding, (error, data) => fn(error, readMatriz(data)));

const readMatriz = (string) => splitLines(string).map(splitColumns);

const splitLines = (string) => string.split(/[\n]+\s*/m).filter(it => it != '');

const splitColumns = (linha) =>
    linha.split(/\s+/g).filter(it => it != '');



const vDiff = (a,b) => (vector) => Math.abs(parseFloat(vector[a]) - parseFloat(vector[b]));

const weatherSpread = vDiff(MAX_TEMP, MIN_TEMP);

const balance = vDiff(F_COLUMN, A_COLUMN);


const min = (fn, key) => (matriz) => matriz.sort((a,b) => fn(a) - fn(b))[0][key]; 

const minWeatherSpread = min(weatherSpread, DAY_NUMBER);

const minBalance = min(balance, TEAM_NAME);


const football = (output) => {
    readFileAsMatriz('football.dat', 'utf8', function (err, matriz) {
        const matriz_filtered = matriz.filter((line) => line.length > 1).splice(1)
        const result = minBalance(matriz_filtered)
        
        output(result)
    });
}

const weather = (output) => {
    readFileAsMatriz('weather.dat', 'utf8', function (err, matriz) {
        const result = minWeatherSpread(matriz.splice(1))

        output(result)
    });
}

module.exports = {
    splitColumns,
    splitLines,
    readMatriz,
    readFileAsMatriz,
    weatherSpread,
    minWeatherSpread,
    minBalance,
    football,
    weather
};


