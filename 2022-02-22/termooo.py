from enum import Enum
import unicodedata
import re

class Feedback(Enum):
    MISS = 1
    RIGHT_PLACE = 2
    WRONG_PLACE = 3

def strip_accents(text):
    text = unicodedata.normalize('NFD', text)
    text = text.encode('ascii', 'ignore')
    text = text.decode('utf-8')
    return str(text)


def remove_miss(letter, words):
    return [word for word in words if letter not in word]


def remove_right_place(index, letter, words):
    return [word for word in words if letter == word[index]]


def remove_wrong_place(index, letter, words):
    return [word for word in words if letter in word and letter != word[index]]

def load_words(file):
    is_ok = re.compile('^[a-z]{5}$')
    with open(file,'r', encoding='utf-8') as stream:
        words = map(str.strip, stream.readlines())
        words = map(str.lower, words)
        words = map(strip_accents, words)
        words = filter(lambda word : is_ok.match(word), words)
        return list(words)

class Termooo():

    def __init__(self, words):
        self.words = list(map(str.lower, words))

    def absent_word(self):
        self.words = self.words[1:]

    def next_word(self):
        ranking = dict( ( k, 0) for k in self.words)
        freq = dict( ( k, 0) for k in 'abcdefghijklmnopqrstuvwxyz')
        for letter in freq.keys():
            for word in self.words:
                if letter in word:
                    freq[letter] = freq.get(letter, 0) + 1
        for word in self.words:
            points = 0
            for letter, frequency in freq.items():
                if letter in word:
                    points += frequency
            ranking[word] = points
        sorted_words = sorted(ranking.items(), key=lambda it: it[1], reverse=True)
        self.words = list(map(lambda it: it[0], sorted_words))
        return sorted_words[0][0]

    def feedback(self, word):
        for index, feed in enumerate(word):
            letter, feedback = feed
            letter = letter.lower()
            if feedback == Feedback.MISS:
                self.words = remove_miss(letter, self.words)
            if feedback == Feedback.RIGHT_PLACE:
                self.words = remove_right_place(index, letter, self.words)
            if feedback == Feedback.WRONG_PLACE:
                self.words = remove_wrong_place(index, letter, self.words)
