from unittest import TestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from termooo import Termooo, Feedback, load_words
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def expand_shadow_element(driver, element):
  shadow_root = driver.execute_script('return arguments[0].shadowRoot', element)
  return shadow_root    

classes_map = {
    'wrong': Feedback.MISS,
    'place': Feedback.WRONG_PLACE,
    'right' : Feedback.RIGHT_PLACE,
}

def class_to_feedback(classes):
    classes = set(classes.split())
    for k,v in classes_map.items():
        if k in classes:
            return v
    raise Exception(f'wrong classes {classes}')

def as_feedback(line, atual): 
    items = line.find_elements(By.CSS_SELECTOR, '.letter')
    for c, item in zip(atual, items):
        yield c, class_to_feedback(item.get_attribute('class'))


class SeleniumTest(TestCase):
    def test(self):

        

        termooo = Termooo(
            sorted(list(set(load_words('br-utf8.txt') + load_words('palavras.txt'))))
        )

        driver = webdriver.Chrome()
        driver.get("https://term.ooo/")
        wait = WebDriverWait(driver, 10)
        
        wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, "main wc-board#board0")))
   
        body = driver.find_element_by_css_selector('body')
            
        board = expand_shadow_element(driver, body.find_element_by_css_selector('main wc-board#board0'))

                
        body.send_keys(Keys.ESCAPE)

        for line in range(6):
            atual = termooo.next_word()
            body.send_keys(atual)
            body.send_keys(Keys.RETURN)
            time.sleep(2)
            line_element = expand_shadow_element(driver, board.find_element(By.CSS_SELECTOR,f'[termo-row="{line}"]'))
            feedback = list(as_feedback(line_element, atual))
            print(feedback)
            termooo.feedback(feedback)

        time.sleep(5)

        
        driver.close()
    