from unittest import TestCase
from termooo import Termooo, Feedback, remove_miss, remove_right_place, remove_wrong_place, load_words


class Test(TestCase):


    def test_run_today(self):
        termooo = Termooo(
            sorted(list(set(load_words('br-utf8.txt') + load_words('palavras.txt'))))
        )
        word = termooo.next_word()
        print(word)
        termooo.feedback([
            ('e', Feedback.MISS),
            ('o', Feedback.MISS),
            ('l', Feedback.MISS),
            ('i', Feedback.WRONG_PLACE),
            ('a', Feedback.MISS),
        ])
        word = termooo.next_word()
        print(word)
        termooo.feedback([
            ('t', Feedback.MISS),
            ('i', Feedback.RIGHT_PLACE),
            ('c', Feedback.MISS),
            ('u', Feedback.RIGHT_PLACE),
            ('m', Feedback.MISS),
        ])
        word = termooo.next_word()
        print("Ignore:", word)
        termooo.absent_word()
        word = termooo.next_word()
        print(word) # \o/

    def test_all_words(self):
        termooo = Termooo(
            sorted(list(set(load_words('br-utf8.txt') + load_words('palavras.txt'))))
        )
        word = termooo.next_word()
        self.assertEqual(word, 'eolia')

    def test_first_word(self):
        termooo = Termooo([
            'AEIOU',
            'AAAAA',
            'EEEEE',
            'IIIII',
            'OOOOO',
            'UUUUU',
        ])
        word = termooo.next_word()
        self.assertEqual(word, 'aeiou')

    def test_second_word(self):
        termooo = Termooo([
            'AEIOU',
            'AAAAA',
            'EEEEE',
            'IIIII',
            'OOOOO',
            'UUUUU',
        ])
        termooo.feedback(
            [
                ('a', Feedback.MISS),
                ('u', Feedback.RIGHT_PLACE),
                ('d', Feedback.MISS),
                ('i', Feedback.MISS),
                ('o', Feedback.MISS),
            ]
        )
        word = termooo.next_word()
        self.assertEqual(word, 'uuuuu')

    def test_remove_miss(self):
        words = [
            'aaaaa',
            'eeeee',
        ]
        words = remove_miss('a', words)
        self.assertEqual(['eeeee'], words)

    def test_remove_right_place(self):
        words = [
            'aeiou',
            'ouiea',
        ]
        words = remove_right_place(1, 'e', words)
        self.assertEqual(['aeiou'], words)

    def test_remove_wrong_place(self):
        words = [
            'aeiou',
            'ouiea',
            'aaaaa',
        ]
        words = remove_wrong_place(0, 'a', words)
        self.assertEqual(['ouiea'], words)



class WordListTest(TestCase):

    def test_read_file(self):
        words = load_words('test.txt')
        expected = [
            'audio',
            'adeus',
            'navio',
        ]
        self.assertEqual(
            words, expected
        )
