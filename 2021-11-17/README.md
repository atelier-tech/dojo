
## Participantes

Ettore Leandro Tognoli
Leandro Ragni
Leonardo Lima

## Expectadores

Louise Ribeiro
Diego Pereira

```python

roman(1)
# I

roman(3)
# III

roman(10)
# X

roman(5)
# V

roman(7)
# VII

roman(15)
# XV

```

## Referência


1 	I
5 	V
10 	X
50 	L
100 	C
500 	D
1000 	M
