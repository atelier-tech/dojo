from __future__ import annotations
from logging import FATAL

class Rational:

    numerator: int

    denominator: int

    def __init__(self, numerator: int, denominator: int = 1) -> None:
        self.numerator = numerator
        self.denominator = denominator
    
    def __str__(self):
        return f"{self.numerator}/{self.denominator}"

    def __mul__(self, other: Rational) -> Rational:
        return Rational(
            self.numerator * other.numerator,
            self.denominator * other.denominator,
        )

    def __truediv__(self, other: Rational) -> Rational:
        return Rational(
            self.numerator * other.denominator,
            self.denominator * other.numerator,
        )

    def common_denominator(self, other: Rational) -> int:
        return self.denominator * other.denominator

    def __add__(self, other: Rational) -> Rational:
        return Rational(
            (self.numerator * other.denominator) +
            (other.numerator * self.denominator),
            self.common_denominator(other),
        )

    def __sub__(self, other: Rational) -> Rational:
        return Rational(
            (self.numerator * other.denominator) -
            (other.numerator * self.denominator),
            self.common_denominator(other),
        )

    def change_denominator(self, new_denominator: int) -> Rational:
        return Rational(new_denominator / self.denominator * self.numerator, new_denominator)

    def __eq__(self, other: object) -> bool:
        if isinstance(other, (int, float,)):
            other = Rational(other)
        if not isinstance(other, Rational):
            return False
        common = self.common_denominator(other)
        a = self.change_denominator(common)
        b = other.change_denominator(common)

        return a.numerator == b.numerator

    def __float__(self) -> float:
        return self.numerator / self.denominator

    