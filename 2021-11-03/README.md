
#

## Participantes

- Ettore Leandro Tognoli
- Leandro Ragni
- Otavio Gomes
- Leonardo Lima
- Flavio Licorio


```python
one = fraction(1)
two = fraction(2)
half = fraction(1 , 2)

half == one / two # True
print(half)
# "1/2"
quarter = half * half
print(quarter)
# "1/4"
float(quarter)
# 0.25

# 10 / 3 = 3.33333
```