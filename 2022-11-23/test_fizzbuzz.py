from unittest import TestCase
from fizzbuzz import fizzbuzz

# TDD - test driven development


class TestSoma(TestCase):

    def test_3(self):
        numero = 3
        self.assertEqual(fizzbuzz(numero), 'fizz')
    
    def test_4(self):
        numero = 4
        self.assertEqual(fizzbuzz(numero), str(numero))

    def test_5(self):
        numero = 5
        self.assertEqual(fizzbuzz(numero), 'buzz')

    def test_15(self):
        numero = 15
        self.assertEqual(fizzbuzz(numero), 'fizzbuzz')
    
    def test_25(self):
        numero = 25
        self.assertEqual(fizzbuzz(numero),'buzz')
        
    def test_multiplos_de_3(self):
        for numero in range(3, 100, 3):
            self.assertIn('fizz', fizzbuzz(numero), f'falha com o numero {numero}')

    def test_multiplos_de_5(self):
        for numero in range(5, 100, 5):
            self.assertIn('buzz', fizzbuzz(numero), f'falha com o numero {numero}')
    
    def test_multiplos_de_3_e_5(self):
        for numero in range(15, 300, 15):
            self.assertIn('fizzbuzz', fizzbuzz(numero), f'falha com o numero {numero}')

    def test_multiplos_de_3(self):
        for numero in range(3, 100, 3):
            self.assertIn('fizz', fizzbuzz(numero), f'falha com o numero {numero}')